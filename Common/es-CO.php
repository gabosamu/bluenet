<?php

	/**
	 * @version 1.0
	 * @description Deberá contener todas las definiciones de frases y palabras que se usarán en el 
	 *              desarrollo orientado a el idioma base que referencia el archivo según los 
	 *              estándares [I18N]
	 *            * Cada definición se agrupará en orden alfabético descendente.
	 * @author Orlando Mavisoy Guerrero
	 * @date 24/03/2017
	 * 
	 */
	
	const _LANG_CODE = 'es-CO';
	const _DATE_SP   = '/';
	const _TIME_SP   = ':';

		////////////////////
		// LANGUAGE START //
		////////////////////

		# A
		const accept                = 'Aceptar';
		const account 				= 'Fondo';
		
		
		# B
		const badge                 = 'Divisa';
		
		
		# C
		const cancel                = 'Cancelar';
		
		
		# D
		const denomination          = 'Denominación';
		
		# E
		const edit                  = 'Editar';
		
		
		# F
		
		# G
		
		# H
		
		# I
		
		# J
		
		# K
		
		# L
		const login                 = 'Ingresar';
		
		# M
		
		# N
		const na                    = 'No aplica';
		
		
		# O
		const office                = 'Oficina';
		
		# P
		const password              = 'contraseña';
		
		
		# Q
		const quality               = 'Calidad';
		
		
		# R
		const ready                 = 'Listo';
		
		# S
		const save 					= 'Guardar';
		
		
		# T
		const total                 = 'Total';
		
		
		# U
		const username              = 'usuario';
		
		# V
		const value                 = 'Valor';
		
		# W
		
		# X
		
		# Y
		
		# Z
		
		////////////////////
		//  LANGUAGE END  //
		////////////////////
?>