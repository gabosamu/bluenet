<?php
    require_once("../Configuration/Connection/Connection.php");      

    class loginDao{
        public $conexion;
        public $res;
        public function __construct(){
            $con = new Connection();
            $this->conexion = $con->Connect();      
        }

        //  * @description Metodo que valida acceso para el login
        //  * @author Orlando Mavisoy Guerrero
        //  * @date 08/01/2019

        public function queryLogin($user, $pass){    
           try{				
                session_start();			
				$stmt = $this->conexion->prepare("CALL buscarUsuarioLogin (?, ?);");
				$stmt->bindParam("1", $user, PDO::PARAM_STR, 4000);
				$stmt->bindParam("2", $pass, PDO::PARAM_STR, 4000); 					
					
				$stmt->execute();  
				$this->res = "false"; 
				if($fila = $stmt->fetch(PDO::FETCH_ASSOC)){	                      		
                        $_SESSION["id"] = $fila["id"];
                        $_SESSION["name"] = $fila["name"];
                        $_SESSION["lastname"] = $fila["lastname"];
                        $this->res = "true";
                        return $this->res;
				}else{
                    return $this->res;
                }					
			}catch(Exception $e){
                die('Error: '. $e->getMessage());
                return false;
			}finally{
                $this->conexion = null;
			}           
        }
    } 
?>