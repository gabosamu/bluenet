        /**          
         * @description Función que envia mediante ajax los datos del usuario
         * @author Orlando Mavisoy Guerrero
         * @date 08/01/2019
         */ 

        $(document).ready(function(){          

            $.extend({
                login: function(data){
                    $.ajax({
                        type: "POST",
                        data: data,  
                        url: "./Controller/loginController.php", 
                        beforeSend: function () {
                            $("#dLoader").show();
                            $("#login").hide();
                        },
                        complete: function () {
                            $('#dLoader').hide();
                        },                                
                        success: function (data) {
                            if(data=="true")
                            {           
                                $("#dLoader").show();           
                                window.location.href = "./View/main.php";	                           
                            }else{ 
                                $("#dLoader").hide();                       
                                alert('Usuario y/o contraseña incorrecto'); 
                                $("#login").show();   	
                            }                                 
                        },
                        error : function(data) {                                                      
                            alert('Disculpe, existió un problema'); 
                        }
                    })                   
                }               
            });       
            
              /**          
             * @description Función que valida los datos del usuario
             * @author Orlando Mavisoy Guerrero
             * @date 08/01/2019
             */ 
                             
            $("#enter").on('click', function() {
                alert("bien");
                window.location.href = "./View/main.php";	 	
                // var user = $("#user").val();
                // var pass = $("#password").val();                
                // var data={
                //     option: "enter",
                //     user: user,
                //     pass: pass,					
                // };
        
                // if(user.length=="")
                //     {
                //       alert("El campo usuario es obligatorio");
                //     }else if(pass.length=="")
                //     {
                //         alert("El campo contraseña es obligatorio");
                //     }else{                
                //         $.login(data);        
                //     }				
            });          
        });