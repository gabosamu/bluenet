//attention
//warning
//danger
//good


var simpleNotify = {
    NOTIFICATION_CLASS_NAME : "simple-notification",
    CONTAINER_CLASS_NAME : "simple-notification-container",
    CONTAINER_ID_NAME : "notificationContainer",
    MARGIN_BETWEEEN_NOTIFICATIONS : 5, 
    NOTIFICATION_WIDTH: 300, 
    NOTIFICATION_TIME : 2000, 
    notificationCount : 0,
  
    notify : function(message, level) {
      var level = typeof level !== 'undefined' ?  level : "good";
      simpleNotify.notificationCount++;
      var notificationId = 'notification' + simpleNotify.notificationCount;
      var newNotification = {"id": notificationId, "message": message, "level": level };
  
       if(document.getElementById("notificationContainer")) {
         simpleNotify.showNotification(newNotification);
       } else {
  
         simpleNotify.createContainer();
         simpleNotify.showNotification(newNotification);
       }
    },
  
    createContainer : function() {
        var divContainer = document.createElement("div");
        divContainer.className = simpleNotify.CONTAINER_CLASS_NAME;
        divContainer.id = simpleNotify.CONTAINER_ID_NAME;
        document.body.appendChild(divContainer);
    },
  
    showNotification : function(notification) {
  
        var newNotification = document.createElement("div");
        newNotification.className = simpleNotify.NOTIFICATION_CLASS_NAME + " " + notification.level;
        newNotification.id = notification.id;
        newNotification.innerHTML = notification.message + "<div class='close-notification' onclick='simpleNotify.close(\"" + newNotification.id + "\")'>x</div>";
        //newNotification.style.marginLeft = simpleNotify.NOTIFICATION_WIDTH + 10 + "px";
  
        var notificationWrapper = document.createElement("div");
        notificationWrapper.className = "simple-notification-wrapper";
        notificationWrapper.appendChild(newNotification);
  
        var notificationContainer = document.getElementById(simpleNotify.CONTAINER_ID_NAME);
        notificationContainer.insertBefore(notificationWrapper, notificationContainer.firstChild);
  
        setTimeout(function() {
          simpleNotify.removeNotification(newNotification.id);
        }, simpleNotify.NOTIFICATION_TIME);
    },
  
    close : function(notification) {
      simpleNotify.removeNotification(notification);
    },
  
    removeNotification : function(notificationToRemove) {
      notificationToRemove = document.getElementById(notificationToRemove);
      try {
        notificationToRemove.className = notificationToRemove.className + " fade-out";
      notificationToRemove.addEventListener("transitionend", function(){
            document.getElementById(simpleNotify.CONTAINER_ID_NAME).removeChild(notificationToRemove.parentElement);
      },false);
      } catch(e) {}
    }
  }
  