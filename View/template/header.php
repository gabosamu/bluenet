<?php
 if (!isset($_SESSION)) {
    session_start();
  }
if(isset ($_SESSION['id'])) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Anuncios</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../Resources/style/bootstrap.min.css">
  <link rel="stylesheet" href="../Resources/style/datatablesBootstrap.min.css">
  <link rel="stylesheet" href="../Resources/style/bootstrap-select.min.css">
  <link rel="stylesheet" href="../Resources/css/estilos.css">
  <script src="../Resources/pluggin/jquery.min.js"></script>
  <script src="../Resources/pluggin/bootstrap.min.js"></script> 
  <script src="../Resources/pluggin/datatablesJquery.min.js"></script>
  <script src="../Resources/pluggin/datatablesBootstrap.min.js"></script>
  <script src="../Resources/pluggin/bootstrap-select.min.js"></script>
  <script src="../Resources/pluggin/all.min.js"></script>
  <script src="../Resources/js/datatables.js"></script>
 
</head>
<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">Logo de la empresa</a>
    </div>
    <ul class="nav navbar-nav">
	  <li class="active"><a href="main.php">Inicio</a></li>
	  <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Usuarios<span class="caret"></span></a>
        <!-- <ul class="dropdown-menu">
          <li><a href="sale.php">Factura de venta</a></li>  
          <li><a href="payment.php">Pagos recibidos</a></li>                  
        </ul> -->
      </li>
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Anuncios<span class="caret"></span></a>
        
      </li>
    
        
    </li>
    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Categoria<span class="caret"></span></a>
       
    </li>   
    
    </ul>
    <ul class="nav navbar-nav navbar-right">
     
      <li><a href="../index.php"><i class="fas fa-user-tie"></i> Salir</a></li>
    </ul>
  </div>
   
</nav>
<p style="color: black; text-align: right; font-weight:bold; padding-right: 20px;">
<?php  
echo "Usuario: ".  $_SESSION["name"].' '.$_SESSION["lastname"] ; ?> </p>
<?php
}else{
    echo "<script>window.location='../index.php';</script>";  
} 
?>




