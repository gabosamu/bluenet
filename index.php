 <!DOCTYPE html>
 <html lang="en">
 <head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="Resources/style/bootstrap.min.css">	
 </head>
 <style type="text/css">
    body {
		font-family: 'Varela Round', sans-serif;
	}
	.modal-login {
		color: #636363;
		width: 350px;
	}
	.modal-login .modal-content {
		padding: 20px;
		border-radius: 5px;
		border: none;
	}
	.modal-login .modal-header {
		border-bottom: none;
		position: relative;
		justify-content: center;
	}
	.modal-login h4 {
		text-align: center;
		font-size: 26px;
	}
	.modal-login  .form-group {
		position: relative;
	}
	.modal-login i {
		position: absolute;
		left: 13px;
		top: 11px;
		font-size: 18px;
	}
	.modal-login .form-control {
		padding-left: 40px;
	}
	.modal-login .form-control:focus {
		border-color: #00ce81;
	}
	.modal-login .form-control, .modal-login .btn {
		min-height: 40px;
		border-radius: 3px; 
	}
	.modal-login .hint-text {
		text-align: center;
		padding-top: 10px;
	}
	.modal-login .close {
        position: absolute;
		top: -5px;
		right: -5px;
	}
	.modal-login .btn {
		background: #00ce81;
		border: none;
		line-height: normal;
	}
	.modal-login .btn:hover, .modal-login .btn:focus {
		background: #00bf78;
	}
	.modal-login .modal-footer {
		background: #ecf0f1;
		border-color: #dee4e7;
		text-align: center;
		margin: 0 -20px -20px;
		border-radius: 5px;
		font-size: 13px;
		justify-content: center;
	}
	.modal-login .modal-footer a {
		color: #999;
	}
	.trigger-btn {
		display: inline-block;
		margin: 100px auto;
	}

	.loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
}

/* Safari */
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

#dLoader { 	 
	display: none;
}

</style>
 <body>
<center>
<div id="dLoader" >
	<h3>Cargando</h3>
	<div class="loader"></div>
</div>
</center>
<div class="modal-dialog modal-login" id="login">
	<div class="modal-content">	
		<div class="modal-header">				
			<h4 class="modal-title">Inicio de sesión</h4>				
		</div>
		<div class="modal-body">
				<label for="name"></label>
				<input type="text" id="user" class="form-control" placeholder="Nombre de Usuario">
				<br>
				<label for="password"></label>
				<input type="password" id="password"  class="form-control"  placeholder="Contraseña">
				<br>						
				<input type="button" id="enter"  class="btn btn-primary btn-block btn-lg" value="Acceder">		
				<br>
		</div>
		<div class="modal-footer">
				<a href="#">Olvidó su contraseña?</a>
		</div>	
	</div>
</div>
<!--<script  src = " //ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js "  type = " text / javascript " > </script>-->
<script src="Resources/pluggin/jquery-2.0.3.min.js"></script>
<script src="Resources/pluggin/bootstrap.min.js"></script> 
<script src="Resources/js/login.js"></script>	
 </body>
 </html>


